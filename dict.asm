extern string_equals

section .text

global find_word

find_word:
	
    .loop:
        add rsi, 0x8        
        push rdi            ; сохраняем регистры
        push rsi            ; сохраняем регистры
        call string_equals  ; сравниваем
        pop rsi             ; достаём регистры
        pop rdi             ; достаём регистры

        cmp rax, 0x1          ; ставим флаги
        je .ans_adress      ; если равенство то возвращаем адрес 

        sub rsi, 0x8        ; берём следующу ссылку
        mov r10, [rsi]      ; кладём ссылку на след вхождение
        mov rsi, r10        ; кладём чтобы rsi имело ссылку на следующие данные

        test r10, r10       ; проверяем на конец
        jnz .loop           ; если не ноль тогда идём дальше

	.ans_zero:
        mov rax, 0          
        ret
        
    .ans_adress:
        sub rsi, 0x8        ; берём адрес
        mov rax, rsi        ; возвращаем значение через rax
        ret

    
