section .text

%define EXIT 60
%define NEW_STRING 10
%define STDIN 0
%define STDOUT 1
%define STDERR 2
%define SYMB_ASCII 48
%define MINUS 45
%define TAB 9
%define SPACE 32
%define MAX 58
%define MIN 47

global exit
global string_length
global print_char
global print_string
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_message
global error


; Принимает код возврата и завершает текущий процесс
exit:
    mov     rax, EXIT
    
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину

            
string_length:
    xor   rax, rax
    
.loop:
    cmp  byte [rdi + rax], 0
    je   .end
    inc  rax
    jmp .loop
.end:
    ret



; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, STDOUT
    mov rdi, STDOUT
    mov rdx, STDOUT
    syscall
    pop rdi
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:

	push rdi             
    call string_length   
    pop rdi
    mov rsi, rdi             
    mov rdx, rax           
    mov rax, STDOUT          
    mov rdi, STDOUT             
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_STRING
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды
print_uint:

    xor rcx, rcx
    xor rax, rax
    dec rsp
    mov [rsp], al
    mov r10, NEW_STRING
    mov rax, rdi
    
.loop:

    xor rdx, rdx
    div r10
    add dl, SYMB_ASCII
    dec rsp
    mov [rsp], dl
    inc rcx
    test rax, rax
    jnz .loop
    
    mov rdi, rsp
    push rcx
    
    call print_string
    
    pop rcx
    add rsp, rcx
    inc rsp
    
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:

    cmp rdi, 0x0
    jge .arn ; значит число положительное
    
    mov r10, rdi
    mov rdi, MINUS
    push r10
    
    call print_char
    
    mov rdi, r10
    neg rdi
    pop r10
    
.arn:
    call print_uint ;выводим число
    
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:

    push r10
    push rdi
    push rsi
    
    call string_length
    mov r10, rax   ;получаем длину 1-ой строки
    mov rdi, rsi
    
    call string_length
    mov r8, rax   ;получаем длину 2-ой строки
    
    pop rsi
    pop rdi
    
    cmp r10, r8 ;сравниваем на длину
    je .el
    
.nel:
    pop r10
    mov rax, 0x0
    ret
       
.el:
    cmp r10, 0x0
    je .ex
    cmpsb 		;сравнение последовательности байтов из участка памяти
    jne .nel 	;если строки не равны
    dec r10
    loop .el
    
    
.ex:
    pop r10
    mov rax, 1
    
    ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:

    push 0x0
    mov rax, STDIN   
    mov rdi, STDIN  
    mov rsi, rsp 
    mov rdx, 1
    
    syscall
    
    pop rax
    
    ret

    
        

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:

	xor rcx, rcx
	xor r10, r10
	xor r8, r8
	
.loop 
	
	push rcx
	push rsi
	push rdi
	call read_char
	pop rdi
	pop rsi
	pop rcx
	
	cmp rcx, rsi ; если буфер меньше слова
	jge .err
	
	cmp rax, 0x0 ; если конец
	je .finish
	
	cmp rcx, 0x0
	jne .check
	
	cmp rax, SPACE
	je .loop
	
	cmp rax, TAB
	je .loop
	
	cmp rax, NEW_STRING
	je .loop
	
.check
	cmp rax, SPACE
    je .finish

    cmp rax, TAB
    je .finish

    cmp rax, NEW_STRING
    je .finish
    
    mov [rdi+rcx], rax
    inc rcx
    jmp .loop	
	
.finish
	mov byte[rdi+rcx], 0x0
	mov rdx, rcx
	mov rax, rdi
	ret
	
.err 
	xor rax, rax
	ret
	


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:



    mov r10, NEW_STRING
    xor r8, r8
    xor rcx, rcx
    xor rax, rax

   
    
    
    
    .num:               
        cmp byte[rdi+rcx], MIN    
        jb .not_symb             
        cmp byte[rdi+rcx], MAX    
        ja .not_symb
        
    ;Если не проходит проверку то мы переходим к 
    ;not.symb иначе мы идём в .enter                                               
    
    
    .enter:
        mul r10                     
        mov r8b, byte[rdi+rcx]      
        sub r8b, SYMB_ASCII               
        add rax, r8                
        inc rcx
        jmp .num
        ; переходим к .num 
    
    
    .not_symb:
        mov rdx, rcx
                 
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

parse_int:
	xor r10, r10
	xor rdx, rdx


    cmp byte [rdi], MINUS
    jne .arn ; если беззнаковое
    
    
    inc rdi
    
    
    call parse_uint
    
    cmp rdx, 0x0
    je .ex
    
    neg rax
    inc rdx
    jmp .ex
    
.arn:
    call parse_uint
    
.ex:
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
    xor rax, rax
    xor r10, 0x0
    
.arn
    cmp rax, rdx
    je .narn
    
    mov bl, byte [rdi + rax]
    mov byte [rsi + rax], bl
    cmp byte [rsi + rax], 0x0
    je .ex
    
    inc rax
    jmp .arn
    
.narn:
    xor rax, rax
    
.ex:
    ret

print_message:
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rdi, rcx
    mov rax, STDOUT  
    syscall
    ret

error:
    mov rcx, STDERR          
    jmp print_message
