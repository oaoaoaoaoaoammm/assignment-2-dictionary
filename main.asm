global _start

%include "colon.inc"
%include "words.inc"
%include "lib.inc"

%define buffer_size 0xFF

extern find_word

section .data
error_size: db "Превышен размер буфера <= 255", 0
error_name: db "Нет значения по такому ключу", 0

buffer: times buffer_size db 0


section .text

_start:
    mov rdi, buffer               ; начало буфера
    mov rsi, buffer_size          ; размер буфера
    call read_word                ; после в rax - адрес слова, rdx - длина слова
    test rax, rax                 ; проверка на размер
    jz .error_of_size             ; если 0 то выводим ошибку

    mov rdi, rax                  ; адрес ключа
    mov rsi, next_element         ; в rsi кладем указатель на следующуее вхождение??
    push rdx                      ; сохраняем регистр
    call find_word                ; в rax результат
    pop rdx                       ; возвращаем регистр
    test rax, rax                 ; проверка на значение по ключу
    je .error_of_name             ; если равны выводим ошибку

    mov rdi, rax                  ; кладем адрес нужного вхождения в rdi
    add rdi, 0x8                  ; добавляем 8 (пропускаем указатель на след вхождение)
    add rdi, 0x1                     
    add rdi, rdx                  ; пропускаем ключ
    call print_string             ; печатаем

    .end:
      call print_newline          ; переводим строку
      xor rdi, rdi
      call exit                   ; завершаем программу

    .error_of_size:
        mov rdi, error_size       ; передаём строку
        call error          	   ; печатаем ошибку
        jmp .end

    .error_of_name:
        mov rdi, error_name       ; передаём строку
        call error                ; печатаем ошибку
        jmp .end
