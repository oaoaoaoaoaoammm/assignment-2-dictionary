%define next_element 0

%macro colon 2
    %ifid %2
        %2: dq next_element
    %else
        %error "2 аргумент это идентификатор"
    %endif
    %ifstr %1
        db %1, 0
    %else
        %error "1 это строка"
    %endif
    %define next_element %2
%endmacro
